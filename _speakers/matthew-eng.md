---
name: Matthew Eng
talks:
- "Building quality data pipelines"
---
Matthew Eng is a marketing and healthcare data scientist and analytics
engineer. He enables marketing and healthcare teams to use data efficiently
by automating the cleansing, transformation, and analysis of data pipelines.
Matthew has experience in insurance, pharmaceuticals, and social media
marketing. He is passionate about using data to make healthcare in the US
simpler and more transparent in order to reduce costs.

Matthew graduated from the University of the Notre Dame with a PhD in
molecular biology and Binghamton University with a B.S. in biological
sciences.
