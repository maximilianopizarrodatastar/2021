---
name: Anastasiia Tymoshchuk
talks:
- "Continuous Documentation for your code"
---
She works in the development for more than 9 years (around 7 years in
Python), including experience in e-commerce as well as game development.
Every day she deals with lots of challenges when she has to consider
software or library to start with, starting from the question how to build
architecture and finishing with a deployment. She has also a great
experience in the design and development of the project from scratch to
production.
