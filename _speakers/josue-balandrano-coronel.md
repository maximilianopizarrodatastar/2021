---
name: Josue Balandrano Coronel
talks:
- "The human side of Software"
---
Hi, my name is Josué, and I'm a human software crafter and a socio-technical
system navigator. I spend much of my time trying to understand how we can
improve the system we are a part of that creates software and how it affects
society.
