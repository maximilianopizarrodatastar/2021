---
name: Reshama Shaikh
talks:
- "Animating Visualizations in Python"
---
Reshama is an independent data scientist/statistician with skills in Python,
R and SAS.

She has a background as a biostatistician in the pharmaceutical industry,
where she worked for over 10 years. She also taught math and statistics for
2 years at Temple University.

She is an organizer of the meetups groups Data Umbrella and NYC PyLadies.

Her interests include:  statistics, deep learning, python, open source,
diversity & inclusion, education, technology, leadership and strategic
change.
