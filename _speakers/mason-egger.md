---
name: Mason Egger
talks:
- "Write Docs Devs Love: Ten Tricks To Level Up Your Tech Writing"
---
Mason Egger is a Sr. Site Reliability Engineer at HomeAway, where he works
on the internal PaaS that hosts all of HomeAway's websites, data streaming,
and tooling. He is an avid programmer, musician, educator, and
writer/blogger at [mason.dev](https://mason.dev). In his spare time he
enjoys camping, kayaking, and exploring new places.
