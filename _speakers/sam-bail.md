---
name: Sam Bail
talks:
- "The Wonderful World of Data Quality Tools in Python"
---
Sam Bail is a Data Insights Engineer at Flatiron Health, a healthcare
technology startup based on New York City. Sam is the co-founder of the UK
based group Manchester Girl Geeks and a PyLadies NYC co-organizer.
