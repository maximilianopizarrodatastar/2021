---
name: Bruno Gonçalves
talks:
- "Graphs for Data Science with NetworkX"
---
Bruno Gonçalves is currently a Senior Data Scientist working at the
intersection of Data Science and Finance. Previously, he was a Data Science
fellow at NYU's Center for Data Science while on leave from a tenured
faculty position at Aix-Marseille Université. Since completing his PhD in
the Physics of Complex Systems in 2008 he has been pursuing the use of Data
Science and Machine Learning to study Human Behavior. He's the author of the
weekly [Sunday Briefing](https://data4sci.com/past-issues) newsletter and
writes regularly at [Graphs For Science](https://graphs4sci.substack.com/)
and [Visualization For Science](https://viz4sci.substack.com/).
