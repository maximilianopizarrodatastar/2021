---
name: Manojit Nandi
talks:
- "Assessing and mitigating unfairness in AI systems"
---
I am a senior data scientist at Microsoft Research where I work on problems
related to algorithmic fairness. I love thinking about how to use data
science and machine learning to tackle problems with high societal impact.
