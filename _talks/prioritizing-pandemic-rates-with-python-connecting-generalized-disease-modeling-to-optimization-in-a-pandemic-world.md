---
duration: 10
presentation_url:
room: Online
slot: 2021-10-01 12:10:00-04:00
speakers:
- Madelyn Kapfhammer
title: "Prioritizing Pandemic Rates with Python: Connecting generalized disease modeling to optimization in a pandemic world"
type: talk
video_url: "https://youtu.be/SzgrpIQsCIY"
---
A backbone of traditional epidemiology is the Susceptible-Infected-Recovered
(SIR) model for infectious disease spread. SIR models allow for the
mathematical depiction and interpretation of infection dynamics with three
general parameters of susceptible, infected, and recovered individuals for a
given disease system. While SIR models are a unique and commonly used type
of infectious disease modeling, uncertainty is almost always present in
determining optimal control of pandemics. At the initial outbreak of a
disease, disease rates--including transmission rate, recovery rate, death
rate, and host arrival rate--are often unknown, making it difficult for
researchers to understand the ideal end results. Consequently, then public
health and medical officials are unable to make informed decisions on
treatment options and safety measures to put in place to ensure the health
of the affected populations. There has been a push to predict ideal disease
rates associated with a specific system, allowing researchers to devise
proper public health protocols to achieve these rates and the highest
amounts of recovered individuals for an outbreak. With a clear indication of
how a disease is transmitted, how individuals in a population interact, and
how hosts recover/suffer from infection and proper modeling, interventions
can combat disease outbreaks, epidemics, and pandemics. As it is difficult
to create SIR models with optimal disease transmission, recovery, death, and
host arrival rates, PandemicPriority is a generalized tool which
computationally determines optimal values for each of these rates through
randomized search for the future development of infectious disease
treatments and interventions.

This talk will outline my journey as a student in Biology and Computer
Science while I was struggling to combine my passions in both fields. How is
it that Python can connect to traditional disease modeling? PandemicPriority
gave me the opportunity to understand the complexities of Python programming
for optimizing disease models, paving the way towards my interest in
computational epidemiology.
