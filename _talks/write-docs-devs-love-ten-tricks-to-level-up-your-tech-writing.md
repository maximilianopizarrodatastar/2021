---
duration: 25
presentation_url:
room: Online
slot: 2021-10-02 13:10:00-04:00
speakers:
- Mason Egger
title: "Write Docs Devs Love: Ten Tricks To Level Up Your Tech Writing"
type: talk
video_url: "https://youtu.be/-4JwlAI-1L0"
---
Think of that feeling you get when you follow an online tutorial or
documentation and the code works on the first run. Now think of all the
hours spent wasted following broken, outdated, or incomplete documentation.
From our favorite tutorials to bad product docs we all consume technical
writing. Tutorials, blog posts, and product docs help developers learn new
things, build projects, and debug issues. But what makes one tutorial better
than another? In this talk I'll discuss how you can write the documentation
that developers love and I’ll share 10 tips and tricks to improve your
technical writing.
