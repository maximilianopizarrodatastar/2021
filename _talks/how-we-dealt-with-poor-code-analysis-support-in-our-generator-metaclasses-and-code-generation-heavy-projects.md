---
duration: 25
presentation_url:
room: Online
slot: 2021-10-01 13:05:00-04:00
speakers:
- Vladimir Losev
title: "How we dealt with poor code analysis support in our generator, metaclasses, and code generation heavy projects"
type: talk
video_url: "https://youtu.be/5FCawJNxF40"
---
Projects that heavily rely on such Python features as metaclasses,
decorators, or code-generation face a problem of poor support by static code
analysis tools. This results in little to no suggestions in IDE or broken
mypy tests.

We'll talk about how we addressed this issue on our open-source
[https://github.com/Toloka/toloka-kit](https://github.com/Toloka/toloka-kit)
and [https://github.com/Toloka/crowd-kit](https://github.com/Toloka/crowd-kit)
projects.
