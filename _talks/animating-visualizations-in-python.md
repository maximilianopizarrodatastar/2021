---
duration: 25
presentation_url:
room: Online
slot: 2021-10-01 11:00:00-04:00
speakers:
- Reshama Shaikh
title: "Animating Visualizations in Python"
type: talk
video_url: "https://youtu.be/XjuoGQWX8Ak"
---
[Plotly](https://plotly.com/python/) is a versatile, open-source python
graphing library which lets us make interactive graphs.  It is built on top
of the [Plotly JavaScript library](plotly.js).  In this talk, I will show
how to go from a typical static visualization to one enriched with
animations.

Here is an [example of an animated line
plot](https://twitter.com/reshamas/status/1404881262867779586) using plotly.

I will share how you can get started creating your own animated plot in
python!
