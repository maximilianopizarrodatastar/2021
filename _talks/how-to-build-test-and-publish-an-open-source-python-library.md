---
duration: 10
presentation_url:
room: Online
slot: 2021-10-02 12:30:00-04:00
speakers:
- Simon Willison
title: "How to build, test and publish an open source Python library"
type: talk
video_url: "https://youtu.be/VMnLXynUqys"
---
I currently maintain over 100 Python packages on PyPI. This is a lot! The
secret to managing this many packages is comprehensive automation powered by
GitHub Actions.

I'll show you how to use cookiecutter, pytest and GitHub Actions to develop a new Python library, add tests, run continuous
integration and automatically  publish new releases of
your package to PyPI.

[Detailed notes to accompany this talk](https://github.com/simonw/pygotham-packaging)
