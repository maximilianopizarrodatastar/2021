---
duration: 25
presentation_url:
room: Online
slot: 2021-10-02 14:25:00-04:00
speakers:
- Jay Miller
title: "Making Government Useable by anyone with Pandas, Python and Eland"
type: talk
video_url: "https://youtu.be/QeO3L6BJSgk"
---
Unreadable data can lead to bad perceptions and positive action being
delayed. Some of the most obscure data comes from reporting from local,
state and federal governments. That said the data is there, freely available
and it has to be provided in a consistent (at some level) manner.

This talk will look at how I made Police Call Records easier to investigate with
Python, Pandas and Eland.
