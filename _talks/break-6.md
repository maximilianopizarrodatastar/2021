---
duration: 180
room: Online
slot: 2021-10-02 15:00:00-04:00
title: End of first showing of Day 2's talks. Tune in in three hours for a repeat showing.
type: break
---
