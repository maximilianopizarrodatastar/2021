---
duration: 25
presentation_url:
room: Online
slot: 2021-10-01 10:00:00-04:00
speakers:
- Manojit Nandi
title: "Assessing and mitigating unfairness in AI systems"
type: talk
video_url: "https://youtu.be/9T91U4RjLnk"
---
While fairness has become an increasingly popular topic in machine learning
and data science, many data scientist struggle with how to incorporate
fairness assessments and unfairness techniques into their work. We will
cover formal frameworks for assessing ML systems for fairness-related harms
and how to apply different algorithmic techniques for mitigating unfairness
in trained models.
