---
duration: 25
presentation_url:
room: Online
slot: 2021-10-02 14:00:00-04:00
speakers:
- Josue Balandrano Coronel
title: "The human side of Software"
type: talk
video_url: "https://youtu.be/ehbYNGlrkL8"
---
We live in a world of systems, and being software developers, we focus on
the technical things we build. But, we often forget the bigger system that
we are a part of. A system with technical and human parts that we have to
navigate daily. We need to acknowledge this to truly understand Software and
everything around it.

In this talk I'll go over the different ways our own preconceptions and
humanity affects how we build software. By doing this we can see the entire
system we are a part of from up above, understand , and navigate it better
to ultimately change it for the better.
