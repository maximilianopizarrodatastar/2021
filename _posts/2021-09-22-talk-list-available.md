---
title: Talk list now available!
date: 2021-09-21 10:00:00 -0400
excerpt_separator: <!--more-->
---

The PyGotham TV 2021 [talk list](/talks/) is now available. It features 22
talks spread across October 1st and 2nd.

<!--more-->

This year's program features a single track with 23 speakers,
giving talks covering a wide array of topics, from
application design to data science to natural language processing to web
development, and featuring our youngest speaker ever at only 13 years old!

We're really excited about this year's talks and hope to have a schedule
ready soon.

Thanks to everyone who proposed a talk and voted on talks.
