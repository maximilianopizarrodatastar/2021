module Jekyll
  module CopyWithDelay
    def copy_with_delay(talks)
      delayed_talks = []
      talks.each do |talk|
        delayed_talk = talk.clone
        delayed_talk.instance_variable_set(:@data, delayed_talk.data.clone)
        delayed_talk.data["slot"] += 60*60*8  # Add 8 hours in seconds
        delayed_talks << delayed_talk
      end
      return delayed_talks
    end
  end
end

Liquid::Template.register_filter(Jekyll::CopyWithDelay)
